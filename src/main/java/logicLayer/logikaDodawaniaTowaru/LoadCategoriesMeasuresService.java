package logicLayer.logikaDodawaniaTowaru;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.JednostkaMiaryEntity;
import domainObject.entity.KategoriaEntity;
import logicLayer.utils.IDAO;
import presentationLayer.dodawanieTowaru.model.ILoadCategoriesMeasuresService;

import java.util.List;

/**
 * Serwis do wczyczywania kategorii i jednostek miary
 */
public class LoadCategoriesMeasuresService implements ILoadCategoriesMeasuresService {
    private IDAO<KategoriaEntity> kategoriaEntityIDAO;
    private IDAO<JednostkaMiaryEntity> jednostkaMiaryEntityIDAO;


    public LoadCategoriesMeasuresService(
            IDAO<KategoriaEntity> kategoriaEntityIDAO,
            IDAO<JednostkaMiaryEntity> jednostkaMiaryEntityIDAO) {
        this.kategoriaEntityIDAO = kategoriaEntityIDAO;
        this.jednostkaMiaryEntityIDAO = jednostkaMiaryEntityIDAO;
    }


    /**
     * Zwraca listę wszystkich kategorii przechowoywancych w bazie danych
     * @return lista wszystkich kategorii, pusta lista jeśli niczego nie znaleziono
     */
    @Override
    public @NotNull
    List<KategoriaEntity> getAllCategory() {
        return kategoriaEntityIDAO.getAll();
    }

    /**
     * Zwraca listę wszystkich jednostek miary przechowoywancych w bazie danych
     * @return lista wszystkich jednostek miary, pusta lista jeśli niczego nie znaleziono
     */
    @Override
    public @NotNull
    List<JednostkaMiaryEntity> getAllMeasures() {
        return jednostkaMiaryEntityIDAO.getAll();
    }
}
