package logicLayer.logikaDodawaniaTowaru;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.TowarEntity;
import logicLayer.utils.IDAO;
import presentationLayer.dodawanieTowaru.model.ISaveTowarService;

/**
 * Serwis do zapisywania towarów
 */
public class SaveTowarService implements ISaveTowarService {
    private IDAO<TowarEntity> towarEntityIDAO;

    public SaveTowarService(IDAO<TowarEntity> towarEntityIDAO) {
        this.towarEntityIDAO = towarEntityIDAO;
    }


    /** Zapisuje podany towar do bazy danych
     * @param towarEntity towar do zapisania
     * @return jeśli udało się zapisać do bazy danych
     */
    @Override
    public boolean saveTowar(@NotNull TowarEntity towarEntity) {
        return trySave(towarEntity);
    }

    private boolean trySave(@NotNull TowarEntity towarEntity) {
        return towarEntityIDAO.add(towarEntity);
    }
}
