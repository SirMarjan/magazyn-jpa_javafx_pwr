package logicLayer.logikaDodawaniaTowaru;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.TowarEntity;
import lombok.NoArgsConstructor;
import presentationLayer.dodawanieTowaru.model.ITowarValidateState;
import presentationLayer.dodawanieTowaru.model.ITowarValidatorService;

/**
 * Serwis walidujący towar
 */
@NoArgsConstructor
public class TowarValidatorService implements ITowarValidatorService {
    private TowarEntity towarEntity;

    private final static int MAX_NAME_LENGTH = 24;
    private final static int CODE_LENGTH = 10;

    private boolean validateName() {
        return towarEntity.getName() != null && towarEntity.getName().length() <= MAX_NAME_LENGTH && !towarEntity.getName().isEmpty();
    }

    private boolean validateCode() {
        return towarEntity.getCode() != null && towarEntity.getCode().length() == CODE_LENGTH && !towarEntity.getCode().isEmpty();
    }

    private boolean validateMinAmount() {
        return towarEntity.getMinAmount() != null && towarEntity.getMinAmount() >= 0;
    }

    private boolean validateMaxAmount() {
        return towarEntity.getMaxAmount() != null && towarEntity.getMaxAmount() >= towarEntity.getMinAmount();
    }


    /**
     * Przeprowadza walidację towaru na poziomie nazwy, kodu oraz minimalnej i maksymalnej ilości
     * @param towarEntity dowar do walidacji
     * @return wynik walidacji
     */
    @Override
    public @NotNull
    ITowarValidateState validateTowar(@NotNull TowarEntity towarEntity) {
        this.towarEntity = towarEntity;
        TowarValidateState towarValidateState = new TowarValidateState();

        towarValidateState.setStringNameValid(validateName());
        towarValidateState.setStringCodeValid(validateCode());
        towarValidateState.setMinAmountValid(validateMinAmount());
        towarValidateState.setMaxAmountValid(validateMaxAmount());

        return towarValidateState;
    }
}
