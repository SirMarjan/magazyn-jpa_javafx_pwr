package logicLayer.logikaDodawaniaTowaru;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import presentationLayer.dodawanieTowaru.model.ITowarValidateState;

/**
 * Zawiera informację o validacji towaru
 */
@Getter
@Setter
@EqualsAndHashCode
public class TowarValidateState implements ITowarValidateState {
    private boolean stringNameValid = true;
    private boolean stringCodeValid = true;
    private boolean maxAmountValid = true;
    private boolean minAmountValid = true;

    /**
     * Zwraca sumę logiczną wyników wszystkich walidaji
     * @return true jeśli towar przeszedł wszystkie walidacje
     */
    @Override
    public boolean passValidate() {
        return stringNameValid &&
                stringCodeValid &&
                maxAmountValid &&
                minAmountValid;
    }

}
