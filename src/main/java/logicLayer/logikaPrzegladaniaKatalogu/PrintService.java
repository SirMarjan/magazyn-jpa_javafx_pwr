package logicLayer.logikaPrzegladaniaKatalogu;

import org.jetbrains.annotations.NotNull;
import javafx.print.PrinterJob;
import presentationLayer.przegladanieKatalogu.model.IPrintService;

/**
 * Serwis do drukowania
 */
public class PrintService implements IPrintService {


    /**
     * Wykonuje print job
     * @param printerJob zlecenie drukowania
     */
    @Override
    public void print(@NotNull PrinterJob printerJob) {
        printerJob.endJob();
    }
}
