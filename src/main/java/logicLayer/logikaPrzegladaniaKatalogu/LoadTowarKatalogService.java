package logicLayer.logikaPrzegladaniaKatalogu;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.*;
import logicLayer.utils.IDAO;
import presentationLayer.przegladanieKatalogu.model.ILoadTowarKatalogService;
import presentationLayer.przegladanieKatalogu.model.ITowarDetailsDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Serwis dostarczający liste szczegułów towarów
 */
public class LoadTowarKatalogService implements ILoadTowarKatalogService {
    private IDAO<TowarEntity> towarEntityIDAO;

    public LoadTowarKatalogService(IDAO<TowarEntity> towarEntityIDAO) {
        this.towarEntityIDAO = towarEntityIDAO;
    }


    /**
     * Zwraca listę tówrów ze szczegółami. wyliczana jest ona na podstawie encji Towarów dostarczanych przez DAO Towarów
     * @return zwraca listę tówrów ze szczegółami
     */
    @Override
    public @NotNull
    List<ITowarDetailsDTO> getAllTowarDetails() {
        List<TowarEntity> towarEntityList = towarEntityIDAO.getAll();
        List<ITowarDetailsDTO> towarDetailsList = new ArrayList<>(towarEntityList.size());

        for (TowarEntity towarEntity : towarEntityList) {
            TowarDetailsDTO towarDetails = new TowarDetailsDTO();
            towarDetails.setName(towarEntity.getName());
            towarDetails.setCode(towarEntity.getCode());
            towarDetails.setMaxAmount(towarEntity.getMaxAmount());
            towarDetails.setMinAmount(towarEntity.getMinAmount());

            towarDetails.setMeasureString(towarEntity.getMeasure().toString());

            StringBuilder stringCategoriesBuilder = new StringBuilder();
            for (KategoriaEntity kategoriaEntity : towarEntity.getCategories()) {
                stringCategoriesBuilder.append(kategoriaEntity.getName());
                stringCategoriesBuilder.append(", ");
            }
            stringCategoriesBuilder.delete(stringCategoriesBuilder.length() - 2, stringCategoriesBuilder.length());
            towarDetails.setCategoriesString(stringCategoriesBuilder.toString());

            int storedGoods = 0;
            int depositedGoods = 0;

            for (OkresowyStanTowaruEntity okresowyStanTowaruEntity : towarEntity.getGoodsPeriodStatus()) {
                if (okresowyStanTowaruEntity.getEndPeriodTime() != null) {
                    storedGoods += okresowyStanTowaruEntity.getStoredGoods();
                    depositedGoods += okresowyStanTowaruEntity.getDepositedGoods();
                } else {
                    for (PozycjaEntity pozycjaEntity : okresowyStanTowaruEntity.getItemsOnList()) {
                        if (pozycjaEntity.getDelivery() != null) {
                            if (pozycjaEntity.getDelivery().getDeliveryStatus() == StatusDostawy.ODEBRANE) {
                                storedGoods += pozycjaEntity.getAmountOfStock();
                            }
                        } else if (pozycjaEntity.getOrder().getOrderStatus() == StatusZamowienia.TWORZONE || pozycjaEntity.getOrder().getOrderStatus() == StatusZamowienia.NIEODEBRANE) {
                            depositedGoods += pozycjaEntity.getAmountOfStock();
                        } else if (pozycjaEntity.getOrder().getOrderStatus() == StatusZamowienia.ODEBRANE) {
                            storedGoods -= pozycjaEntity.getAmountOfStock();
                        }
                    }
                }


            }

            towarDetails.setAvailableGoods(storedGoods - depositedGoods);
            towarDetails.setStoredGoods(storedGoods);
            towarDetails.setDepositedGoods(depositedGoods);
            towarDetailsList.add(towarDetails);
        }
        return towarDetailsList;
    }
}
