package logicLayer.logikaPrzegladaniaKatalogu;

import lombok.*;
import presentationLayer.przegladanieKatalogu.model.ITowarDetailsDTO;

/**
 * Klasa zawierająca szczegółowe informację o towarze
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class TowarDetailsDTO implements ITowarDetailsDTO {
    private int id;
    private String name;
    private String code;
    private int maxAmount;
    private int minAmount;
    private int storedGoods;
    private int availableGoods;
    private int depositedGoods;
    private String measureString;
    private String categoriesString;

}


