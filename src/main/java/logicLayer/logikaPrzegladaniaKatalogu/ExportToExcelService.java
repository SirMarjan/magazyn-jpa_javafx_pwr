package logicLayer.logikaPrzegladaniaKatalogu;

import org.jetbrains.annotations.NotNull;
import jxl.Workbook;
import jxl.write.*;
import jxl.write.Number;
import presentationLayer.przegladanieKatalogu.model.IExportToExcelService;
import presentationLayer.przegladanieKatalogu.model.ITowarDetailsDTO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Serwis zapisujący towar do pliku excel
 */
public class ExportToExcelService implements IExportToExcelService {


    /**
     * Zapisuje liste towarów do pliku excel, zastępójąc poprzedni plik jeśli istniał
     * @param towarDetailsList lista towarów do zapisania
     * @param file plik w którym ma zostać zapisana lista towaru
     * @return zwraca true jeśli operacją się powiodła
     */
    @Override
    public boolean exportToExcel(@NotNull List<ITowarDetailsDTO> towarDetailsList, @NotNull File file) {
        WritableWorkbook writableWorkbook;

        boolean isSaved = true;

        try (OutputStream outputStream = new FileOutputStream(file, false)) {
            writableWorkbook = Workbook.createWorkbook(outputStream);
            WritableSheet sheet = prepareWorkbookSheet(writableWorkbook);

            saveTowarDetailsListToSheet(sheet, towarDetailsList);

            writableWorkbook.write();
            writableWorkbook.close();

        } catch (Exception e) {
            e.printStackTrace();
            isSaved = false;
        }

        return isSaved;
    }

    private @NotNull
    WritableSheet prepareWorkbookSheet(@NotNull WritableWorkbook writableWorkbook) throws WriteException {
        WritableSheet excelSheet = writableWorkbook.createSheet("Katatlog", 0);
        excelSheet.addCell(new Label(0, 0, "Kod"));
        excelSheet.addCell(new Label(1, 0, "Nazwa"));
        excelSheet.addCell(new Label(2, 0, "Jednostka miary"));
        excelSheet.addCell(new Label(3, 0, "Maksymalna ilość towaru"));
        excelSheet.addCell(new Label(4, 0, "Minimalna ilość towaru"));
        excelSheet.addCell(new Label(5, 0, "Dostępna ilość towaru"));
        excelSheet.addCell(new Label(6, 0, "Zdeponowana ilość towaru"));
        excelSheet.addCell(new Label(7, 0, "Całkowita ilośc towaru"));
        excelSheet.addCell(new Label(8, 0, "Kategorie"));

        return excelSheet;
    }

    private void saveTowarDetailsListToSheet(@NotNull WritableSheet writableSheet, @NotNull List<ITowarDetailsDTO> towarDetailsDTOList) throws WriteException {
        int row = 1;
        for (ITowarDetailsDTO iTowarDetailsDTO : towarDetailsDTOList) {
            writableSheet.addCell(new Label(0, row, iTowarDetailsDTO.getCode()));
            writableSheet.addCell(new Label(1, row, iTowarDetailsDTO.getName()));
            writableSheet.addCell(new Label(2, row, iTowarDetailsDTO.getMeasureString()));
            writableSheet.addCell(new Number(3, row, iTowarDetailsDTO.getMaxAmount()));
            writableSheet.addCell(new Number(4, row, iTowarDetailsDTO.getMinAmount()));
            writableSheet.addCell(new Number(5, row, iTowarDetailsDTO.getAvailableGoods()));
            writableSheet.addCell(new Number(6, row, iTowarDetailsDTO.getDepositedGoods()));
            writableSheet.addCell(new Number(7, row, iTowarDetailsDTO.getStoredGoods()));
            writableSheet.addCell(new Label(8, row, iTowarDetailsDTO.getCategoriesString()));
            row++;
        }
    }

}
