package logicLayer.utils;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.IEntity;

import java.util.List;

/**
 * Interfejs zapewniający dostęp do danych
 * @param <E> typ encji
 */
public interface IDAO<E extends IEntity> {
    /**
     * Zwraca listę wszystkich obiektów danego typu encji
     * @return lista wszystkich obiektów enjcji. Pusta lista gdy nic nie znalezniono
     */
    List<E> getAll();

    /**
     * Dodaje obiekt encji do bazy danych
     * @param entity encjia do dodania
     * @return true jeśli się powiodło
     */
    boolean add(@NotNull E entity);
}
