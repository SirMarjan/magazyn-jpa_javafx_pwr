import domainObject.entity.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashSet;

public class MainPrepareDB {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CREATE");
        EntityManager entityManager = entityManagerFactory.createEntityManager();


        JednostkaMiaryEntity jednostkaMiaryEntity1 = new JednostkaMiaryEntity("kilogram");
        KategoriaEntity kategoriaEntity1 = new KategoriaEntity("beton");
        KategoriaEntity kategoriaEntity2 = new KategoriaEntity("polski");


        TowarEntity towarEntity = new TowarEntity();
        towarEntity.setName("Beton Vordox");
        towarEntity.setCode("123ASDASDA");
        towarEntity.setMeasure(jednostkaMiaryEntity1);
        towarEntity.setMaxAmount(12);
        towarEntity.setMinAmount(5);
        towarEntity.setCategories(new HashSet<>());
        towarEntity.getCategories().add(kategoriaEntity1);
        towarEntity.getCategories().add(kategoriaEntity2);


        TowarEntity towarEntity2 = new TowarEntity();
        towarEntity2.setName("Beton Alex");
        towarEntity2.setCode("123ASDASDS");
        towarEntity2.setMeasure(jednostkaMiaryEntity1);
        towarEntity2.setMaxAmount(12);
        towarEntity2.setMinAmount(5);
        towarEntity2.setCategories(new HashSet<>());
        towarEntity2.getCategories().add(kategoriaEntity2);

        DostawaEntity dostawaEntity = new DostawaEntity();
        dostawaEntity.setInvoiceNr("123");
        dostawaEntity.setDeliveryStatus(StatusDostawy.ODEBRANE);

        DostawaEntity dostawaEntity2 = new DostawaEntity();
        dostawaEntity2.setInvoiceNr("126");
        dostawaEntity2.setDeliveryStatus(StatusDostawy.NIEODEBRANE);

        PozycjaEntity pozycjaEntity = new PozycjaEntity();
        pozycjaEntity.setAmountOfStock(30);
        pozycjaEntity.setDelivery(dostawaEntity);

        PozycjaEntity pozycjaEntity2 = new PozycjaEntity();
        pozycjaEntity2.setAmountOfStock(20);
        pozycjaEntity2.setDelivery(dostawaEntity2);

        ZamowienieKlientaEntity zamowienieKlientaEntity = new ZamowienieKlientaEntity();
        zamowienieKlientaEntity.setOrderNr("124");
        zamowienieKlientaEntity.setOrderStatus(StatusZamowienia.TWORZONE);

        ZamowienieKlientaEntity zamowienieKlientaEntity2 = new ZamowienieKlientaEntity();
        zamowienieKlientaEntity2.setOrderNr("125");
        zamowienieKlientaEntity2.setOrderStatus(StatusZamowienia.NIEODEBRANE);

        PozycjaEntity pozycjaEntity3 = new PozycjaEntity();
        pozycjaEntity3.setAmountOfStock(10);
        pozycjaEntity3.setOrder(zamowienieKlientaEntity);

        PozycjaEntity pozycjaEntity4 = new PozycjaEntity();
        pozycjaEntity4.setAmountOfStock(10);
        pozycjaEntity4.setOrder(zamowienieKlientaEntity2);

        OkresowyStanTowaruEntity okresowyStanTowaruEntity = new OkresowyStanTowaruEntity();
        okresowyStanTowaruEntity.setAvailableGoods(20);
        okresowyStanTowaruEntity.setDepositedGoods(10);
        okresowyStanTowaruEntity.setStoredGoods(30);
        okresowyStanTowaruEntity.setEndPeriodTime(new Timestamp(1200L));

        OkresowyStanTowaruEntity okresowyStanTowaruEntity2 = new OkresowyStanTowaruEntity();
        okresowyStanTowaruEntity2.setEndPeriodTime(null);
        okresowyStanTowaruEntity2.setItemsOnList(new HashSet<>(Arrays.asList(pozycjaEntity, pozycjaEntity2, pozycjaEntity3, pozycjaEntity4)));

        OkresowyStanTowaruEntity okresowyStanTowaruEntity3 = new OkresowyStanTowaruEntity();
        okresowyStanTowaruEntity3.setAvailableGoods(20);
        okresowyStanTowaruEntity3.setDepositedGoods(10);
        okresowyStanTowaruEntity3.setStoredGoods(30);
        okresowyStanTowaruEntity3.setEndPeriodTime(new Timestamp(1200L));

        towarEntity.setGoodsPeriodStatus(new HashSet<>(Arrays.asList(okresowyStanTowaruEntity, okresowyStanTowaruEntity2)));
        towarEntity2.setGoodsPeriodStatus(new HashSet<>(Arrays.asList(okresowyStanTowaruEntity3)));

        entityManager.getTransaction().begin();
        entityManager.persist(jednostkaMiaryEntity1);
        entityManager.persist(new JednostkaMiaryEntity("tona"));
        entityManager.persist(new JednostkaMiaryEntity("metr"));
        entityManager.persist(new KategoriaEntity("cegła"));
        entityManager.persist(new KategoriaEntity("gips"));
        entityManager.persist(kategoriaEntity1);
        entityManager.persist(kategoriaEntity2);
        entityManager.persist(dostawaEntity);
        entityManager.persist(dostawaEntity2);
        entityManager.persist(zamowienieKlientaEntity);
        entityManager.persist(zamowienieKlientaEntity2);
        entityManager.persist(pozycjaEntity);
        entityManager.persist(pozycjaEntity2);
        entityManager.persist(pozycjaEntity3);
        entityManager.persist(pozycjaEntity4);
        entityManager.persist(okresowyStanTowaruEntity);
        entityManager.persist(okresowyStanTowaruEntity2);
        entityManager.persist(okresowyStanTowaruEntity3);

        entityManager.persist(towarEntity);
        entityManager.persist(towarEntity2);

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();


    }
}
