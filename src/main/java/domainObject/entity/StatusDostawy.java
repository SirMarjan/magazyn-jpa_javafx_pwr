package domainObject.entity;

public enum StatusDostawy {
    ODEBRANE,
    NIEODEBRANE,
    ANULOWANE
}
