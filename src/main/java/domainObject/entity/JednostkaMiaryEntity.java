package domainObject.entity;

import org.jetbrains.annotations.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "JEDNOSTKI_MIARY")
public class JednostkaMiaryEntity implements IEntity {

    public JednostkaMiaryEntity() {
    }

    public JednostkaMiaryEntity(@NotNull String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue
    @Getter
    private Integer id;
    @Column(name = "nazwaJednostkiMiary", unique = true, nullable = false)
    @Getter
    @Setter
    private String name;

    @Override
    public String toString() {
        return getName();
    }
}
