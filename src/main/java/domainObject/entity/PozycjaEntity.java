package domainObject.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "POZYCJE")
public class PozycjaEntity implements IEntity {
    @Id
    @Getter
    @GeneratedValue
    Integer id;

    @Column(nullable = false)
    @Getter
    @Setter
    Integer amountOfStock;

    @ManyToOne
    @Getter
    @Setter
    DostawaEntity delivery;

    @ManyToOne
    @Getter
    @Setter
    ZamowienieKlientaEntity order;

}
