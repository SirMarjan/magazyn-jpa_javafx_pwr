package domainObject.entity;

import org.jetbrains.annotations.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@Entity
@Table(name = "KATEGORIE")
public class KategoriaEntity implements IEntity {

    public KategoriaEntity() {
    }

    public KategoriaEntity(@NotNull String name) {
        this.name = name;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue
    @Getter
    private Integer id;
    @Column(name = "nazwaKategorii", unique = true, nullable = false)
    @Getter
    @Setter
    private String name;

    @Override
    public String toString() {
        return getName();
    }

}
