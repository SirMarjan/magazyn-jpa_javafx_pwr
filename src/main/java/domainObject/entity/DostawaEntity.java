package domainObject.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "DOSTAWY")
public class DostawaEntity implements IEntity {

    @Id
    @Getter
    @GeneratedValue
    Integer id;

    @Column(unique = true, nullable = false)
    @Getter
    @Setter
    String invoiceNr;

    @Column
    @Getter
    @Setter
    java.sql.Date deliveryDate;

    @Column(nullable = false)
    @Getter
    @Setter
    StatusDostawy deliveryStatus;


}
