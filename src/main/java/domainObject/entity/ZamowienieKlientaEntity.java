package domainObject.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ZAMOWIENIEKLIENTA")
public class ZamowienieKlientaEntity implements IEntity {

    @Id
    @GeneratedValue
    @Getter
    Integer id;

    @Getter
    @Setter
    @Column(nullable = false, unique = true)
    String orderNr;

    @Getter
    @Setter
    @Column
    java.sql.Date orderDate;

    @Getter
    @Setter
    @Column(nullable = false)
    StatusZamowienia orderStatus;


}
