package domainObject.entity;

public interface IEntity {
    Integer getId();
}