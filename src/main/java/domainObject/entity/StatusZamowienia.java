package domainObject.entity;

public enum StatusZamowienia {
    TWORZONE,
    NIEODEBRANE,
    ODEBRANE,
    ANULOWANE,
    NIEZREALIZOWANE
}
