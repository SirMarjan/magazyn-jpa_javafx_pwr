package domainObject.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
public class OkresowyStanTowaruEntity implements IEntity {

    @Id
    @Getter
    @GeneratedValue
    private Integer id;

    @Getter
    @Setter
    @Column(nullable = true)
    private java.sql.Timestamp endPeriodTime;

    @Getter
    @Setter
    @Column(nullable = true)
    private int storedGoods;

    @Getter
    @Setter
    @Column(nullable = true)
    private int availableGoods;

    @Getter
    @Setter
    @Column(nullable = true)
    private int depositedGoods;

    @OneToMany
    @Getter
    @Setter
    private Set<PozycjaEntity> itemsOnList;


}
