package domainObject.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "TOWARY")
public class TowarEntity implements IEntity {
    @Id
    @GeneratedValue
    @Getter
    private Integer id;
    @Column(name = "kodTowaru", unique = true, nullable = false)
    @Getter
    @Setter
    private String code;
    @Column(name = "nazwaTowaru", unique = true, nullable = false)
    @Getter
    @Setter
    private String name;
    @Column(name = "maksymalnaIloscTowaru", nullable = false)
    @Getter
    @Setter
    private Integer maxAmount;
    @Column(name = "minimalnaIloscTowaru", nullable = false)
    @Getter
    @Setter
    private Integer minAmount;

    @OneToMany
    @Getter
    @Setter
    private Set<OkresowyStanTowaruEntity> goodsPeriodStatus;

    @ManyToOne
    @JoinColumn(name = "jednostkaMiaryId")
    @Getter
    @Setter
    private JednostkaMiaryEntity measure;

    @ManyToMany
    @JoinTable(name = "Towar_Kategoria",
            joinColumns = {@JoinColumn(name = "towarId")},
            inverseJoinColumns = {@JoinColumn(name = "kategoriaId")})
    @Getter
    @Setter
    Set<KategoriaEntity> categories;
}
