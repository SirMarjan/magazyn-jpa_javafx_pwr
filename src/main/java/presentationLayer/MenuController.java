package presentationLayer;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuController {
    public void onDodawanieTowaruButtonAction(ActionEvent actionEvent) {
        try {
            loadStage("Dodawanie towaru", "dodawanieTowaruView/dodawanie_towaru.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onPrzegladanieKataloguButtonAction(ActionEvent actionEvent) {
        try {
            loadStage("Przeglądanie katalogu", "przegladanieKataloguView/przegladanie_katalogu.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadStage(String title, String resource) throws IOException {
        Parent root;
        root = FXMLLoader.load(getClass().getClassLoader().getResource(resource));
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(new Scene(root, 1400, 700));
        stage.show();
    }
}
