package presentationLayer.przegladanieKatalogu.model;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;
/**
 * Interfejs serwisu zapisującego towar do pliku excel
 */
public interface IExportToExcelService {

    /**
     * Zapisuje liste towarów do pliku excel, zastępójąc poprzedni plik jeśli istniał
     * @param towarDetailsList lista towarów do zapisania
     * @param file plik w którym ma zostać zapisana lista towaru
     * @return zwraca true jeśli operacją się powiodła
     */
    boolean exportToExcel(@NotNull List<ITowarDetailsDTO> towarDetailsList, @NotNull File file);
}
