package presentationLayer.przegladanieKatalogu.model;

import org.jetbrains.annotations.NotNull;

import java.util.List;


/**
 * Interfejs serwisu dostarczającego liste szczegułów towarów
 */
public interface ILoadTowarKatalogService {
    @NotNull
    List<ITowarDetailsDTO> getAllTowarDetails();
}
