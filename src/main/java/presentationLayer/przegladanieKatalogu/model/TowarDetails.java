package presentationLayer.przegladanieKatalogu.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Klasa przechowywyująca szczegóły towaru
 */
@NoArgsConstructor
@EqualsAndHashCode
public class TowarDetails implements ITowarDetailsDTO, Serializable {
    private StringProperty name = new SimpleStringProperty(this, "name");
    private StringProperty code = new SimpleStringProperty(this, "code");
    private IntegerProperty maxAmount = new SimpleIntegerProperty(this, "maxAmount");
    private IntegerProperty minAmount = new SimpleIntegerProperty(this, "minAmount");
    private IntegerProperty storedGoods = new SimpleIntegerProperty(this, "storedGoods");
    private IntegerProperty availableGoods = new SimpleIntegerProperty(this, "availableGoods");
    private IntegerProperty depositedGoods = new SimpleIntegerProperty(this, "depositedGoods");
    private StringProperty measureString = new SimpleStringProperty(this, "measureString");
    private StringProperty categoriesString = new SimpleStringProperty(this, "categoriesString");

    public TowarDetails(ITowarDetailsDTO iTowarDetailsDTO) {
        setName(iTowarDetailsDTO.getName());
        setCode(iTowarDetailsDTO.getCode());
        setMaxAmount(iTowarDetailsDTO.getMaxAmount());
        setMinAmount(iTowarDetailsDTO.getMinAmount());
        setStoredGoods(iTowarDetailsDTO.getStoredGoods());
        setAvailableGoods(iTowarDetailsDTO.getAvailableGoods());
        setDepositedGoods(iTowarDetailsDTO.getDepositedGoods());
        setMeasureString(iTowarDetailsDTO.getMeasureString());
        setCategoriesString(iTowarDetailsDTO.getCategoriesString());
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getCode() {
        return code.get();
    }

    public StringProperty codeProperty() {
        return code;
    }

    public void setCode(String code) {
        this.code.set(code);
    }

    public int getMaxAmount() {
        return maxAmount.get();
    }

    public IntegerProperty maxAmountProperty() {
        return maxAmount;
    }

    public void setMaxAmount(int maxAmount) {
        this.maxAmount.set(maxAmount);
    }

    public int getMinAmount() {
        return minAmount.get();
    }

    public IntegerProperty minAmountProperty() {
        return minAmount;
    }

    public void setMinAmount(int minAmount) {
        this.minAmount.set(minAmount);
    }

    public int getStoredGoods() {
        return storedGoods.get();
    }

    public IntegerProperty storedGoodsProperty() {
        return storedGoods;
    }

    public void setStoredGoods(int storedGoods) {
        this.storedGoods.set(storedGoods);
    }

    public int getAvailableGoods() {
        return availableGoods.get();
    }

    public IntegerProperty availableGoodsProperty() {
        return availableGoods;
    }

    public void setAvailableGoods(int availableGoods) {
        this.availableGoods.set(availableGoods);
    }

    public int getDepositedGoods() {
        return depositedGoods.get();
    }

    public IntegerProperty depositedGoodsProperty() {
        return depositedGoods;
    }

    public void setDepositedGoods(int depositedGoods) {
        this.depositedGoods.set(depositedGoods);
    }

    public String getMeasureString() {
        return measureString.get();
    }

    public StringProperty measureStringProperty() {
        return measureString;
    }

    public void setMeasureString(String measureString) {
        this.measureString.set(measureString);
    }

    public String getCategoriesString() {
        return categoriesString.get();
    }

    public StringProperty categoriesStringProperty() {
        return categoriesString;
    }

    public void setCategoriesString(String categoriesString) {
        this.categoriesString.set(categoriesString);
    }
}
