package presentationLayer.przegladanieKatalogu.model;

import java.io.Serializable;

/**
 * Interfejs zawierający szczegółowe informację o towarze
 */
public interface ITowarDetailsDTO extends Serializable {
    String getName();

    String getCode();

    int getMaxAmount();

    int getMinAmount();

    int getStoredGoods();

    int getAvailableGoods();

    int getDepositedGoods();

    String getMeasureString();

    String getCategoriesString();

}
