package presentationLayer.przegladanieKatalogu.model;

import org.jetbrains.annotations.NotNull;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.print.PrinterJob;
import presentationLayer.przegladanieKatalogu.controller.IPrzegladanieKataloguModel;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class PrzegladanieKataloguModel implements IPrzegladanieKataloguModel {
    private IExportToExcelService iExportToExcelService;
    private ILoadTowarKatalogService iLoadTowarKatalogService;
    private IPrintService iPrintService;
    private ObservableList<TowarDetails> towarDetailsList = FXCollections.observableArrayList();

    public PrzegladanieKataloguModel(IExportToExcelService iExportToExcelService,
                                     ILoadTowarKatalogService iLoadTowarKatalogService,
                                     IPrintService iPrintService) {
        this.iExportToExcelService = iExportToExcelService;
        this.iLoadTowarKatalogService = iLoadTowarKatalogService;
        this.iPrintService = iPrintService;
        refreshTowarKatalog();
    }


    public boolean exportToExcel(@NotNull File file) {
        List<ITowarDetailsDTO> towarDetailsDTOList = new LinkedList<>(getTowarDetailsObservableList());
        return iExportToExcelService.exportToExcel(towarDetailsDTOList, file);
    }

    public ObservableList<TowarDetails> getTowarDetailsObservableList() {
        return towarDetailsList;
    }

    public void print(@NotNull PrinterJob printerJob) {
        iPrintService.print(printerJob);
    }

    private void refreshTowarKatalog() {
        towarDetailsList.clear();
        towarDetailsList.addAll(
                convertITowarDetailsDTOListToTowarDetailObservableList(iLoadTowarKatalogService.getAllTowarDetails()));
    }

    private static @NotNull
    List<TowarDetails> convertITowarDetailsDTOListToTowarDetailObservableList(
            @NotNull List<ITowarDetailsDTO> iTowarDetailsDTOList) {
        return iTowarDetailsDTOList
                .stream()
                .map(TowarDetails::new)
                .collect(Collectors.toList());
    }
}
