package presentationLayer.przegladanieKatalogu.model;


import org.jetbrains.annotations.NotNull;
import javafx.print.PrinterJob;

/**
 * Serwis do drukowania
 */
public interface IPrintService {

    /**
     * Wykonuje print job
     * @param printerJob zlecenie drukowania
     */
    void print(@NotNull PrinterJob printerJob);
}
