package presentationLayer.przegladanieKatalogu.controller;

import javafx.collections.ObservableList;
import javafx.print.PrinterJob;
import presentationLayer.przegladanieKatalogu.model.TowarDetails;

import java.io.File;

public interface IPrzegladanieKataloguModel {

    /**
     * Eksportuje do excela szczegułowe dane towarów
     * @param excelFile plik w którym zostaną zapisane dane
     * @return true jeśli się powiodło
     */
    boolean exportToExcel(File excelFile);

    ObservableList<TowarDetails> getTowarDetailsObservableList();

    /**
     * Rozpoczyna akcję drukowania
     * @param printerJob akcja drukowania
     */
    void print(PrinterJob printerJob);
}
