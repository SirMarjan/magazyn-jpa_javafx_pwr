package presentationLayer.przegladanieKatalogu.controller;

import javafx.fxml.FXML;
import javafx.print.PrinterJob;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import logicLayer.logikaPrzegladaniaKatalogu.ExportToExcelService;
import logicLayer.logikaPrzegladaniaKatalogu.LoadTowarKatalogService;
import logicLayer.logikaPrzegladaniaKatalogu.PrintService;
import persistenceLayer.DAO.TowarDAO;
import presentationLayer.przegladanieKatalogu.model.PrzegladanieKataloguModel;
import presentationLayer.przegladanieKatalogu.model.TowarDetails;

import java.io.File;

public class PrzegladanieKataloguController {
    private IPrzegladanieKataloguModel model = new PrzegladanieKataloguModel(new ExportToExcelService(), new LoadTowarKatalogService(new TowarDAO()), new PrintService());

    @FXML
    private TableView<TowarDetails> tableView;
    @FXML
    private TableColumn<TowarDetails, String> nameColumn;
    @FXML
    private TableColumn<TowarDetails, String> codeColumn;
    @FXML
    private TableColumn<TowarDetails, String> measureColumn;
    @FXML
    private TableColumn<TowarDetails, Integer> maxAmountColumn;
    @FXML
    private TableColumn<TowarDetails, Integer> minAmountColumn;
    @FXML
    private TableColumn<TowarDetails, Integer> availableColumn;
    @FXML
    private TableColumn<TowarDetails, Integer> depositedColumn;
    @FXML
    private TableColumn<TowarDetails, Integer> storedColumn;
    @FXML
    private TableColumn<TowarDetails, String> categoriesColumn;
    @FXML
    Button buttonExit;
    @FXML
    Button buttonExport;


    @FXML
    private void initialize() {
        tableView.setDisable(true);
        initializeTableView();
        tableView.setDisable(false);
    }

    @FXML
    public void buttonPrintOnAction() {
        PrinterJob printerJob = PrinterJob.createPrinterJob();
        if (printerJob.showPrintDialog(tableView.getScene().getWindow()) && printerJob.printPage(tableView))
            model.print(printerJob);
    }

    @FXML
    public void buttonExitOnAction() {
        Stage stage = (Stage) buttonExit.getScene().getWindow();
        stage.close();
    }

    public void buttonExportOnAction() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XLS files (*.xls)", "*.xls");
        fileChooser.getExtensionFilters().add(extFilter);

        File excelFile =
                selectFile(buttonExport.getScene().getWindow(), "XLS files (*.xls)", "*.xls");

        if (excelFile != null) {
            boolean isExportSuccess = model.exportToExcel(excelFile);
            showExportAlert(isExportSuccess);
        }
    }

    private void showExportAlert(boolean success) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Exportowanie");
        alert.setHeaderText(null);
        if (success)
            alert.setContentText("Pomyślnie wyeksportowano katalog");
        else
            alert.setContentText("Błąd podczas eksportowania katalogu");
        alert.showAndWait();
    }

    private File selectFile(Window window, String description, String... extensions) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(description, extensions);
        fileChooser.getExtensionFilters().add(extFilter);
        return fileChooser.showSaveDialog(window);
    }

    private void initializeTableView() {
        nameColumn.setCellValueFactory(value -> value.getValue().nameProperty());
        codeColumn.setCellValueFactory(value -> value.getValue().codeProperty());
        measureColumn.setCellValueFactory(value -> value.getValue().measureStringProperty());
        maxAmountColumn.setCellValueFactory(value -> value.getValue().maxAmountProperty().asObject());
        minAmountColumn.setCellValueFactory(value -> value.getValue().minAmountProperty().asObject());
        availableColumn.setCellValueFactory(value -> value.getValue().availableGoodsProperty().asObject());
        depositedColumn.setCellValueFactory(value -> value.getValue().depositedGoodsProperty().asObject());
        storedColumn.setCellValueFactory(value -> value.getValue().storedGoodsProperty().asObject());
        categoriesColumn.setCellValueFactory(value -> value.getValue().categoriesStringProperty());
        tableView.setItems(model.getTowarDetailsObservableList());
    }


}
