package presentationLayer.dodawanieTowaru.model;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.TowarEntity;

/**
 * Interfejs serwisu do zapisywania towarów
 */
public interface ISaveTowarService {

    /** Zapisuje podany towar do bazy danych
     * @param towarEntity towar do zapisania
     * @return jeśli udało się zapisać do bazy danych
     */
    boolean saveTowar(@NotNull TowarEntity towarEntity);
}
