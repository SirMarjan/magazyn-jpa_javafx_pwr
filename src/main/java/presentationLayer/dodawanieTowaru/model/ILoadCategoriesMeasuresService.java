package presentationLayer.dodawanieTowaru.model;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.JednostkaMiaryEntity;
import domainObject.entity.KategoriaEntity;

import java.util.List;
/**
 * Interfejs serwiu do wczyczywania kategorii i jednostek miary
 */
public interface ILoadCategoriesMeasuresService {
    /**
     * Zwraca listę wszystkich kategorii przechowoywancych w bazie danych
     * @return lista wszystkich kategorii, pusta lista jeśli niczego nie znaleziono
     */
    @NotNull
    List<KategoriaEntity> getAllCategory();

    /**
     * Zwraca listę wszystkich jednostek miary przechowoywancych w bazie danych
     * @return lista wszystkich jednostek miary, pusta lista jeśli niczego nie znaleziono
     */
    @NotNull
    List<JednostkaMiaryEntity> getAllMeasures();
}
