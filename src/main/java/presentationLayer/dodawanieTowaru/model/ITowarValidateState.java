package presentationLayer.dodawanieTowaru.model;


/**
 * Zawiera informację o validacji towaru
 */
public interface ITowarValidateState {

    /**
     * Zwraca sumę logiczną wyników wszystkich walidaji
     * @return true jeśli towar przeszedł wszystkie walidacje
     */
    boolean passValidate();

    boolean isStringNameValid();

    void setStringNameValid(boolean value);

    boolean isStringCodeValid();

    void setStringCodeValid(boolean value);

    boolean isMaxAmountValid();

    void setMaxAmountValid(boolean value);

    boolean isMinAmountValid();

    void setMinAmountValid(boolean value);
}
