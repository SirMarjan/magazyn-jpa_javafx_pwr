package presentationLayer.dodawanieTowaru.model;

import domainObject.entity.KategoriaEntity;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode
public class Kategoria implements Serializable {
    private IntegerProperty id = new SimpleIntegerProperty(this, "id");
    private StringProperty name = new SimpleStringProperty(this, "name");

    public final int getId() {
        return id.get();
    }

    public final void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public final String getName() {
        return name.get();
    }

    public final void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public Kategoria(int id, String name) {
        setId(id);
        setName(name);
    }

    public Kategoria(KategoriaEntity kategoriaEntity) {
        this(kategoriaEntity.getId(), kategoriaEntity.getName());
    }

}
