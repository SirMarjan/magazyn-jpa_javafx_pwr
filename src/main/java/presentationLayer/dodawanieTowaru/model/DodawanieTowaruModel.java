package presentationLayer.dodawanieTowaru.model;


import org.jetbrains.annotations.NotNull;
import domainObject.entity.JednostkaMiaryEntity;
import domainObject.entity.KategoriaEntity;
import domainObject.entity.TowarEntity;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import presentationLayer.dodawanieTowaru.controller.IDodawanieTowaruModel;

import java.util.*;

public class DodawanieTowaruModel implements IDodawanieTowaruModel {
    private TowarEntity towarEntity;

    private Set<KategoriaEntity> kategoriaEntitySet = new HashSet<>();
    private Set<JednostkaMiaryEntity> jednostkaMiaryEntitySet = new HashSet<>();

    private ObservableList<JednostkaMiary> jednostkaMiaryObservableList = FXCollections.observableArrayList();
    private ObservableList<Kategoria> kategoriaObservableList = FXCollections.observableArrayList();
    private ObservableList<Kategoria> chosenKategoriaObservableList = FXCollections.observableArrayList();

    private ObjectProperty<Kategoria> chosenKategoria = new SimpleObjectProperty<>();
    private ObjectProperty<JednostkaMiary> chosenJednostkaMiary = new SimpleObjectProperty<>();

    private StringProperty name = new SimpleStringProperty();
    private StringProperty code = new SimpleStringProperty();
    private StringProperty minAmount = new SimpleStringProperty();
    private StringProperty maxAmount = new SimpleStringProperty();

    private ITowarValidateState iTowarValidateState;

    private ILoadCategoriesMeasuresService iLoadCategoriesMeasuresService;
    private ITowarValidatorService iTowarValidatorService;
    private ISaveTowarService iSaveTowarService;

    public DodawanieTowaruModel(ILoadCategoriesMeasuresService iLoadCategoriesMeasuresService,
                                ISaveTowarService iSaveTowarService,
                                ITowarValidatorService iTowarValidatorService) {
        this.iLoadCategoriesMeasuresService = iLoadCategoriesMeasuresService;
        this.iSaveTowarService = iSaveTowarService;
        this.iTowarValidatorService = iTowarValidatorService;

        reloadCategoriesAndMeasuresData();
    }

    public void addToChosenCategories() {
        Kategoria category = chosenKategoria.get();
        getKategoriaObservableList().remove(category);
        getChosenKategoriaObservableList().add(category);
    }

    public void removeFromChosenCategories(@NotNull List<Kategoria> removedCategories) {
        getKategoriaObservableList().addAll(removedCategories);
        getKategoriaObservableList().sort(Comparator.comparing(Kategoria::getName));
        getChosenKategoriaObservableList().removeAll(removedCategories);
    }

    public void createTowar() {
        iTowarValidateState = null;
        towarEntity = createTowarEntity(name.get(),
                code.get(),
                Integer.parseInt(maxAmount.get()),
                Integer.parseInt(minAmount.get()),
                new LinkedList<>(chosenKategoriaObservableList),
                chosenJednostkaMiary.get());
    }

    public void validateTowar() {
        if (isTowarEntityNull())
            throw new NullPointerException("Towar jest null-em");

        iTowarValidateState = iTowarValidatorService.validateTowar(towarEntity);
    }

    public boolean save() {
        if (isTowarEntityNull())
            throw new NullPointerException("Towar jest null-em");
        if (iTowarValidateState == null)
            throw new IllegalStateException("Towar nie został zwalidowany");
        if (!iTowarValidateState.passValidate())
            throw new IllegalStateException("Towar nie przeszedł walidacji");

        return iSaveTowarService.saveTowar(towarEntity);
    }

    public void clear() {
        towarEntity = null;
        iTowarValidateState = null;
        name.setValue("");
        code.setValue("");
        maxAmount.setValue("");
        minAmount.setValue("");

        refreshCategoriesAndMeasures();
    }

    public @NotNull
    String getWarning() {
        if (iTowarValidateState == null)
            throw new IllegalStateException("Towar nie jest zwalidowany");
        StringBuilder stringBuilder = new StringBuilder();
        if (!iTowarValidateState.isStringNameValid())
            stringBuilder.append("Zła nazwa między 1 a 24 znaki\n");
        if (!iTowarValidateState.isStringCodeValid())
            stringBuilder.append("Kod musi mieć 9 znaków\n");
        if (!iTowarValidateState.isMinAmountValid())
            stringBuilder.append("Minimalna ilość musi być większa lub równa zero\n");
        if (!iTowarValidateState.isMaxAmountValid())
            stringBuilder.append("Maksymalna ilość musi być większa lub równa ilości minimalnej");

        return stringBuilder.toString();
    }

    public void reloadCategoriesAndMeasuresData() {
        if (!kategoriaEntitySet.isEmpty())
            kategoriaEntitySet.clear();
        if (!jednostkaMiaryEntitySet.isEmpty())
            jednostkaMiaryEntitySet.clear();

        kategoriaEntitySet.addAll(iLoadCategoriesMeasuresService.getAllCategory());
        jednostkaMiaryEntitySet.addAll(iLoadCategoriesMeasuresService.getAllMeasures());

        refreshCategoriesAndMeasures();
    }

    public ObservableList<JednostkaMiary> getJednostkaMiaryObservableList() {
        return jednostkaMiaryObservableList;
    }

    public ObservableList<Kategoria> getKategoriaObservableList() {
        return kategoriaObservableList;
    }

    public ObservableList<Kategoria> getChosenKategoriaObservableList() {
        return chosenKategoriaObservableList;
    }

    public ObjectProperty<Kategoria> chosenKategoriaProperty() {
        return chosenKategoria;
    }

    public ObjectProperty<JednostkaMiary> chosenJednostkaMiaryProperty() {
        return chosenJednostkaMiary;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty codeProperty() {
        return code;
    }

    public StringProperty minAmountProperty() {
        return minAmount;
    }


    public StringProperty maxAmountProperty() {
        return maxAmount;
    }

    public ITowarValidateState getITowarValidateState() {
        return iTowarValidateState;
    }

    private void refreshCategoriesAndMeasures() {
        if (!kategoriaObservableList.isEmpty())
            kategoriaObservableList.clear();
        if (!jednostkaMiaryObservableList.isEmpty())
            jednostkaMiaryObservableList.clear();

        for (KategoriaEntity kategoriaEntity : kategoriaEntitySet) {
            kategoriaObservableList.add(new Kategoria(kategoriaEntity));
        }

        for (JednostkaMiaryEntity jednostkaMiaryEntity : jednostkaMiaryEntitySet) {
            jednostkaMiaryObservableList.add(new JednostkaMiary(jednostkaMiaryEntity));
        }

        kategoriaObservableList.sort(Comparator.comparing(Kategoria::getName));
        jednostkaMiaryObservableList.sort(Comparator.comparing(JednostkaMiary::getName));

        chosenKategoriaObservableList.clear();
    }

    private @NotNull
    TowarEntity createTowarEntity(@NotNull String name,
                                  @NotNull String code,
                                  int maxAmount,
                                  int minAmount,
                                  @NotNull List<Kategoria> kategoriaList,
                                  @NotNull JednostkaMiary jednostkaMiary) {
        TowarEntity towarEntity = new TowarEntity();
        towarEntity.setName(name);
        towarEntity.setCode(code);
        towarEntity.setMaxAmount(maxAmount);
        towarEntity.setMinAmount(minAmount);
        towarEntity.setMeasure(jednostkaMiaryToEntity(jednostkaMiary));
        towarEntity.setGoodsPeriodStatus(new HashSet<>());

        List<KategoriaEntity> kategoriaEntityList = new LinkedList<>();
        for (Kategoria kategoria : kategoriaList) {
            kategoriaEntityList.add(kategoriaToEntity(kategoria));
        }

        towarEntity.setCategories(new HashSet<>(kategoriaEntityList));

        return towarEntity;
    }

    private @NotNull
    KategoriaEntity kategoriaToEntity(@NotNull Kategoria kategoria) {
        return kategoriaEntitySet.stream().filter(x -> x.getId().equals(kategoria.getId())).findFirst().get();
    }

    private @NotNull
    JednostkaMiaryEntity jednostkaMiaryToEntity(@NotNull JednostkaMiary jednostkaMiary) {
        return jednostkaMiaryEntitySet.stream().filter(x -> x.getId().equals(jednostkaMiary.getId())).findFirst().get();
    }

    private boolean isTowarEntityNull() {
        return towarEntity == null;
    }


}


