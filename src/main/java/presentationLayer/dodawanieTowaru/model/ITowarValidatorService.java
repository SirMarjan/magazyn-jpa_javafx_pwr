package presentationLayer.dodawanieTowaru.model;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.TowarEntity;
/**
 * Interfejs serwisu walidującego towar
 */
public interface ITowarValidatorService {


    /**
     * Przeprowadza walidację towaru na poziomie nazwy, kodu oraz minimalnej i maksymalnej ilości
     * @param towarEntity dowar do walidacji
     * @return wynik walidacji
     */
    @NotNull
    ITowarValidateState validateTowar(@NotNull TowarEntity towarEntity);
}
