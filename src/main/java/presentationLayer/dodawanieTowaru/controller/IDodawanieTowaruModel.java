package presentationLayer.dodawanieTowaru.controller;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import presentationLayer.dodawanieTowaru.model.ITowarValidateState;
import presentationLayer.dodawanieTowaru.model.JednostkaMiary;
import presentationLayer.dodawanieTowaru.model.Kategoria;

import java.util.List;

public interface IDodawanieTowaruModel {

    /**
     * Dodaje wybraną kategorie do wybranych kategorii
     */
    void addToChosenCategories();

    /** Usuwa kategorie z wybranych kategorii
     * @param removedCategories lista kategorii do usunięcia
     */
    void removeFromChosenCategories(List<Kategoria> removedCategories);

    /**
     * Tworzy towar w modelu
     */
    void createTowar();

    /**
     * Waliduje towar w modeu
     */
    void validateTowar();

    /**
     * Zapisuje towar z modelu do bazy danych
     * @return true jeśli się udało, false jeśli nie unikalne id
     */
    boolean save();

    /**
     * Czyśli stan modelu
     */
    void clear();

    /**
     * Zwraca listę ostrzeżeń z poprzedniej walidacji
     * @return tekst zawierający ostrzerzenia
     */
    String getWarning();

    /**
     * Wczytuje dane o kategoriach i jednostkach miary z bazy danych
     */
    void reloadCategoriesAndMeasuresData();

    ObservableList<JednostkaMiary> getJednostkaMiaryObservableList();

    ObservableList<Kategoria> getKategoriaObservableList();

    ObservableList<Kategoria> getChosenKategoriaObservableList();

    ObjectProperty<Kategoria> chosenKategoriaProperty();

    ObjectProperty<JednostkaMiary> chosenJednostkaMiaryProperty();

    StringProperty nameProperty();

    StringProperty codeProperty();

    StringProperty minAmountProperty();

    StringProperty maxAmountProperty();

    ITowarValidateState getITowarValidateState();


}
