package presentationLayer.dodawanieTowaru.controller;


import org.jetbrains.annotations.NotNull;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import logicLayer.logikaDodawaniaTowaru.LoadCategoriesMeasuresService;
import logicLayer.logikaDodawaniaTowaru.SaveTowarService;
import logicLayer.logikaDodawaniaTowaru.TowarValidatorService;
import persistenceLayer.DAO.JednostkaMiaryDAO;
import persistenceLayer.DAO.KategoriaDAO;
import persistenceLayer.DAO.TowarDAO;
import presentationLayer.dodawanieTowaru.model.DodawanieTowaruModel;
import presentationLayer.dodawanieTowaru.model.ITowarValidateState;
import presentationLayer.dodawanieTowaru.model.JednostkaMiary;
import presentationLayer.dodawanieTowaru.model.Kategoria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DodawanieTowaruController {
    private DodawanieTowaruModel model = new DodawanieTowaruModel(new LoadCategoriesMeasuresService(new KategoriaDAO(), new JednostkaMiaryDAO()), new SaveTowarService(new TowarDAO()), new TowarValidatorService());

    @FXML
    public TextField codeTF;
    @FXML
    public TextField nameTF;
    @FXML
    public TextField minAmountTF;
    @FXML
    public TextField maxAmountTF;
    @FXML
    public ComboBox<JednostkaMiary> measureCB;
    @FXML
    public ComboBox<Kategoria> categoryCB;
    @FXML
    public Button addTowarButton;
    @FXML
    public Button exitButton;
    @FXML
    public Button addCategoryButton;
    @FXML
    public Button removeCategoryButton;
    @FXML
    public ListView<Kategoria> categoryLV;

    private List<TextField> textFieldList;


    @FXML
    public void initialize() {
        textFieldList = Arrays.asList(codeTF, nameTF, minAmountTF, maxAmountTF);

        measureCB.setItems(model.getJednostkaMiaryObservableList());
        measureCB.getSelectionModel().selectFirst();
        categoryCB.setItems(model.getKategoriaObservableList());
        categoryCB.getSelectionModel().selectFirst();
        categoryLV.setItems(model.getChosenKategoriaObservableList());
        configureControls();
    }

    @FXML
    public void addTowarButtonAction() {
        boolean isControlsEmpty = isMarkControlsEmpty();
        if (!isControlsEmpty) {
            model.createTowar();
            model.validateTowar();

            ITowarValidateState iTowarValidateState = model.getITowarValidateState();
            if (!iTowarValidateState.passValidate()) {
                editStyleOfWarning(iTowarValidateState);
                showWarningAlert();
            } else {
                boolean isSaved = model.save();
                if (isSaved) {
                    showAddAlert();
                    clearData();
                    editStyleOfNotEmptyTextField(textFieldList);
                    categoryLV.getStyleClass().remove("error");
                } else {
                    codeTF.getStyleClass().add("error");
                    showAddErrorWarning();
                }
            }

        }
    }

    @FXML
    public void exitButtonAction() {
        Stage stage = (Stage) exitButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void addCategoryButtonAction() {
        model.addToChosenCategories();
        categoryCB.getSelectionModel().selectFirst();
        if (model.getKategoriaObservableList().isEmpty()) {
            categoryCB.setDisable(true);
            addCategoryButton.setDisable(true);
        }
    }

    @FXML
    public void removeCategoryButtonAction() {
        List<Kategoria> removedCategories = categoryLV.getSelectionModel().getSelectedItems();
        if (!removedCategories.isEmpty()) {
            model.removeFromChosenCategories(removedCategories);
            categoryCB.getSelectionModel().selectFirst();
            categoryCB.setDisable(false);
            addCategoryButton.setDisable(false);
        }
    }

    private void editStyleOfWarning(@NotNull ITowarValidateState iTowarValidateState) {
        if (!iTowarValidateState.isStringNameValid())
            nameTF.getStyleClass().add("error");
        if (!iTowarValidateState.isMinAmountValid())
            minAmountTF.getStyleClass().add("error");
        if (!iTowarValidateState.isMaxAmountValid())
            maxAmountTF.getStyleClass().add("error");
    }

    private void showWarningAlert() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Błąd");
        alert.setContentText(model.getWarning());
        alert.showAndWait();
    }

    private void showAddAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Dodano towar");
        alert.setHeaderText(model.nameProperty().get());
        alert.setContentText("Został dodadny do bazy");
        alert.showAndWait();
    }

    private void showAddErrorWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Nie dodano towaru");
        alert.setHeaderText(model.nameProperty().get());
        alert.setContentText("Towar powinien mieć unikalne id");
        alert.showAndWait();
    }

    private void clearData() {
        model.clear();
        categoryCB.getSelectionModel().selectFirst();
        measureCB.getSelectionModel().selectFirst();
    }

    private boolean isMarkControlsEmpty() {
        List<TextField> emptyTextFieldList = getEmptyTextFiledList();
        boolean isEmptyTextFieldListEmpty = emptyTextFieldList.isEmpty();

        List<TextField> notEmptyTextFieldList = new ArrayList<>(textFieldList);
        notEmptyTextFieldList.removeAll(emptyTextFieldList);
        boolean isNotEmptyTextFieldListEmpty = notEmptyTextFieldList.isEmpty();

        if (!isEmptyTextFieldListEmpty)
            editStyleOfEmptyTextField(emptyTextFieldList);
        if (!isNotEmptyTextFieldListEmpty)
            editStyleOfNotEmptyTextField(notEmptyTextFieldList);

        boolean isChosenCategoryListEmpty = editStyleCategoryListView();

        return isChosenCategoryListEmpty || !isEmptyTextFieldListEmpty;
    }

    private boolean editStyleCategoryListView() {
        boolean isChosenCategoryListEmpty = isChosenCategoryListEmpty();
        if (isChosenCategoryListEmpty)
            categoryLV.getStyleClass().add("error");
        else
            categoryLV.getStyleClass().remove("error");
        return isChosenCategoryListEmpty;
    }

    private boolean isChosenCategoryListEmpty() {
        return model.getChosenKategoriaObservableList().isEmpty();
    }

    private @NotNull
    List<TextField> getEmptyTextFiledList() {
        return textFieldList
                .stream()
                .filter(x -> x.getText().isEmpty())
                .collect(Collectors.toList());
    }

    private void configureControls() {
        final Callback<ListView<JednostkaMiary>, ListCell<JednostkaMiary>> measureCBCellFactory = new Callback<ListView<JednostkaMiary>, ListCell<JednostkaMiary>>() {
            @Override
            public ListCell<JednostkaMiary> call(ListView<JednostkaMiary> param) {
                return new ListCell<JednostkaMiary>() {
                    @Override
                    protected void updateItem(JednostkaMiary jednostkaMiary, boolean empty) {
                        super.updateItem(jednostkaMiary, empty);
                        if (jednostkaMiary != null) {
                            setText(jednostkaMiary.getName());
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        };

        final Callback<ListView<Kategoria>, ListCell<Kategoria>> categoryCBCellFactory = new Callback<ListView<Kategoria>, ListCell<Kategoria>>() {
            @Override
            public ListCell<Kategoria> call(ListView<Kategoria> param) {
                return new ListCell<Kategoria>() {
                    @Override
                    protected void updateItem(Kategoria kategoria, boolean empty) {
                        super.updateItem(kategoria, empty);
                        if (kategoria != null) {
                            setText(kategoria.getName());
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        };

        final ListCell<JednostkaMiary> measureCBButtonCell = new ListCell<JednostkaMiary>() {
            @Override
            protected void updateItem(JednostkaMiary jednostkaMiary, boolean empty) {
                super.updateItem(jednostkaMiary, empty);
                if (jednostkaMiary != null) {
                    setText(jednostkaMiary.getName());
                } else {
                    setText(null);
                }
            }
        };

        final ListCell<Kategoria> categoryCBButtonCell = new ListCell<Kategoria>() {
            @Override
            protected void updateItem(Kategoria kategoria, boolean empty) {
                super.updateItem(kategoria, empty);
                if (kategoria != null) {
                    setText(kategoria.getName());
                } else {
                    setText(null);
                }
            }
        };


        measureCB.setCellFactory(measureCBCellFactory);
        categoryCB.setCellFactory(categoryCBCellFactory);
        measureCB.setButtonCell(measureCBButtonCell);
        categoryCB.setButtonCell(categoryCBButtonCell);

        categoryLV.setCellFactory(categoryCBCellFactory);
        categoryLV.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        maxAmountTF.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*") || newValue.length() > 9) {
                maxAmountTF.setText(oldValue);
            }
        });

        minAmountTF.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*") || newValue.length() > 9) {
                minAmountTF.setText(oldValue);
            }
        });

        nameTF.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 24) {
                nameTF.setText(oldValue);
            }
        });

        codeTF.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 10) {
                codeTF.setText(oldValue);
            }
        });

        model.chosenJednostkaMiaryProperty().bind(measureCB.getSelectionModel().selectedItemProperty());
        model.chosenKategoriaProperty().bind(categoryCB.getSelectionModel().selectedItemProperty());

        model.nameProperty().bindBidirectional(nameTF.textProperty());
        model.codeProperty().bindBidirectional(codeTF.textProperty());
        model.minAmountProperty().bindBidirectional(minAmountTF.textProperty());
        model.maxAmountProperty().bindBidirectional(maxAmountTF.textProperty());
    }

    private static void editStyleOfEmptyTextField(List<TextField> emptyTextFieldList) {
        emptyTextFieldList
                .forEach(textField -> textField.getStyleClass().add("error"));

    }

    private static void editStyleOfNotEmptyTextField(@NotNull List<TextField> notEmptyTextFieldList) {
        notEmptyTextFieldList
                .forEach(textField -> textField.getStyleClass().remove("error"));
    }


}
