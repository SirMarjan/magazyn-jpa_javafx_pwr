package persistenceLayer.utils;

import lombok.Getter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceLayerSingleton {
    private static PersistenceLayerSingleton ourInstance = new PersistenceLayerSingleton();

    public static PersistenceLayerSingleton getInstance() {
        return ourInstance;
    }

    public void close() {
        if (!isClose) {
            getInstance().entityManager.close();
            getInstance().entityManagerFactory.close();
            isClose = true;
        } else
            throw new IllegalStateException("Ponowna próba zamknięcia EntityMenagera");

    }

    @Getter
    private final EntityManagerFactory entityManagerFactory;
    @Getter
    private final EntityManager entityManager;
    @Getter
    private boolean isClose;

    private PersistenceLayerSingleton() {
        entityManagerFactory = Persistence.createEntityManagerFactory("PERSISTENCE");
        entityManager = entityManagerFactory.createEntityManager();
        isClose = false;
    }
}
