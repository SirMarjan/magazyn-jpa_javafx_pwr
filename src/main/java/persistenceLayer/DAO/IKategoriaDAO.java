package persistenceLayer.DAO;

import domainObject.entity.KategoriaEntity;

public interface IKategoriaDAO extends IBasicDAO<KategoriaEntity> {
}
