package persistenceLayer.DAO;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.IEntity;
import logicLayer.utils.IDAO;

import java.util.List;

public interface IBasicDAO<E extends IEntity> extends IDAO<E> {

    /**
     * Zwraca encje o podanym id lub null
     * @param id poszukiwane id encji
     * @return obiekt E encji lub null gdy nic nie znaleziono
     */
    E getById(int id);


    /**
     * Zwraca listę wszystkich obiektów danego typu encji
     * @return lista wszystkich obiektów E enjcji. Pusta lista gdy nic nie znalezniono
     */
    @NotNull
    List<E> getAll();


    /**
     * Dodaje obiekt E encji do bazy danych
     * @param entity encjia do dodania
     * @return true jeśli się powiodło
     */
    boolean add(@NotNull E entity);

    /**
     * Aktualizuje encję E w bazie danych
     * @param entity encja do zaktalizowania
     * @return true jeśli się powiodło
     */
    boolean update(@NotNull E entity);


    /**
     * Usuwa encję E z bazy danych
     * @param entity encja do usunięcia
     * @return true jeśli się powiodło
     */
    boolean remove(@NotNull E entity);

}
