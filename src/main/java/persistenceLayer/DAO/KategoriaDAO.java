package persistenceLayer.DAO;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.KategoriaEntity;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class KategoriaDAO extends ABasicDAO<KategoriaEntity> implements IKategoriaDAO {

    /**
     * Zwraca encję kategori o podanym id lub null
     * @param id poszukiwane id encji
     * @return obiekt encji lub null gdy nic nie znaleziono
     */
    @Override
    public KategoriaEntity getById(int id) {
        return entityManager.find(KategoriaEntity.class, id);
    }

    /**
     * Zwraca listę wszystkich encji kategorji
     * @return lista wszystkich obiektów encji. Pusta lista gdy nic nie znalezniono
     */
    @Override
    public @NotNull
    List<KategoriaEntity> getAll() {
        Query query = entityManager.createQuery("select e from KategoriaEntity e");
        List result = query.getResultList();
        if (result == null)
            result = new ArrayList<KategoriaEntity>();
        return (List<KategoriaEntity>) result;
    }
}
