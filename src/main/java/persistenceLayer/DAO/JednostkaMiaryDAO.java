package persistenceLayer.DAO;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.JednostkaMiaryEntity;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class JednostkaMiaryDAO extends ABasicDAO<JednostkaMiaryEntity> implements IJednostkaMiaryDAO {

    /**
     * Zwraca encję jednostki miary o podanym id lub null
     * @param id poszukiwane id encji
     * @return obiekt encji lub null gdy nic nie znaleziono
     */
    @Override
    public JednostkaMiaryEntity getById(int id) {
        return entityManager.find(JednostkaMiaryEntity.class, id);
    }

    /**
     * Zwraca listę wszystkich encji jednostek miary
     * @return lista wszystkich obiektów encji. Pusta lista gdy nic nie znalezniono
     */
    @Override
    public @NotNull
    List<JednostkaMiaryEntity> getAll() {
        Query query = entityManager.createQuery("select e from JednostkaMiaryEntity e");
        List result = query.getResultList();
        if (result == null)
            result = new ArrayList<JednostkaMiaryEntity>();
        return (List<JednostkaMiaryEntity>) result;
    }
}
