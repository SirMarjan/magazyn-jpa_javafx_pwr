package persistenceLayer.DAO;

import domainObject.entity.TowarEntity;

public interface ITowarDAO extends IBasicDAO<TowarEntity> {
}
