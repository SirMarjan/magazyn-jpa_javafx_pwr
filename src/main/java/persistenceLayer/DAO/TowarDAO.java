package persistenceLayer.DAO;


import org.jetbrains.annotations.NotNull;
import domainObject.entity.TowarEntity;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class TowarDAO extends ABasicDAO<TowarEntity> implements ITowarDAO {

    /**
     * Zwraca encję towaru o podanym id lub null
     * @param id poszukiwane id encji
     * @return obiekt encji lub null gdy nic nie znaleziono
     */
    @Override
    public TowarEntity getById(int id) {
        return entityManager.find(TowarEntity.class, id);
    }


    /**
     * Zwraca listę wszystkich encji towarów
     * @return lista wszystkich obiektów encji. Pusta lista gdy nic nie znalezniono
     */
    @Override
    public @NotNull
    List<TowarEntity> getAll() {
        Query query = entityManager.createQuery("select e from TowarEntity e");
        List result = query.getResultList();
        if (result == null)
            result = new ArrayList<TowarEntity>();
        return (List<TowarEntity>) result;
    }
}
