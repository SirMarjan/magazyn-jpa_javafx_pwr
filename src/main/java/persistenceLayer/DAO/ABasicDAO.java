package persistenceLayer.DAO;

import org.jetbrains.annotations.NotNull;
import domainObject.entity.IEntity;
import persistenceLayer.utils.PersistenceLayerSingleton;

import javax.persistence.EntityManager;

public abstract class ABasicDAO<E extends IEntity> implements IBasicDAO<E> {
    protected EntityManager entityManager = PersistenceLayerSingleton.getInstance().getEntityManager();

    /**
     * Dodaje obiekt encji E do bazy danych
     * @param entity encjia E do dodania
     * @return true jeśli się powiodło
     */
    @Override
    public boolean add(@NotNull E entity) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(entity);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Aktualizuje encję E w bazie danych
     * @param entity encja do zaktalizowania
     * @return true jeśli się powiodło
     */
    @Override
    public boolean update(@NotNull E entity) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(entity);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            return false;
        }
        return true;
    }

    /**
     * Usuwa encję E z bazy danych
     * @param entity encja do usunięcia
     * @return true jeśli się powiodło
     */
    @Override
    public boolean remove(@NotNull E entity) {
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(entity);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            return false;
        }
        return true;
    }
}
