import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import persistenceLayer.utils.PersistenceLayerSingleton;

public class MyMain extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("menuView/menu.fxml"));

        primaryStage.setTitle("Menu");
        primaryStage.setScene(new Scene(root, 1200, 700));
        primaryStage.show();
    }

    @Override
    public void stop() {
        PersistenceLayerSingleton.getInstance().close();
    }
}
