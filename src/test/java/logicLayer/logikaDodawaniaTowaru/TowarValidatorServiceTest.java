package logicLayer.logikaDodawaniaTowaru;

import domainObject.entity.TowarEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import presentationLayer.dodawanieTowaru.model.ITowarValidateState;

import static org.junit.Assert.*;

public class TowarValidatorServiceTest {
    private TowarValidatorService towarValidatorService;
    private ITowarValidateState iTowarValidateState;
    private TowarEntity towarEntity;

    @Before
    public void prepareTowarValidator(){
        towarValidatorService=new TowarValidatorService();
        towarEntity = new TowarEntity();
    }

    @After
    public void clearData(){
        towarValidatorService=null;
        towarEntity=null;
        iTowarValidateState=null;
    }

    @Test
    public void testCorrectNameValidate(){
        towarEntity.setName("Pantene, Pro-V");
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertTrue(iTowarValidateState.isStringNameValid());
    }

    @Test
    public void testMaxLengthNameValidate(){
        towarEntity.setName("AAAAAAAAAAAAAAAAAAAAAAAA");
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertTrue(iTowarValidateState.isStringNameValid());
    }

    @Test
    public void testEmptyNameValidate(){
        towarEntity.setName("");
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertFalse(iTowarValidateState.isStringNameValid());
    }

    @Test
    public void testToBigNameValidate(){
        towarEntity.setName("AAAAAAAAAAAAAAAAAAAAAAAAA");
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertFalse(iTowarValidateState.isStringNameValid());
    }

    @Test
    public void testCorrectCodeValidate(){
        towarEntity = new TowarEntity();
        towarEntity.setCode("EE21AA21EE");
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertTrue(iTowarValidateState.isStringCodeValid());
    }

    @Test
    public void testIncorrectCodeValidate(){
        towarEntity = new TowarEntity();
        towarEntity.setCode("EE21LLLLL");
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertFalse(iTowarValidateState.isStringCodeValid());
    }

    @Test
    public void testCorrectMinAmountValidate(){
        towarEntity.setMinAmount(12);
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertTrue(iTowarValidateState.isMinAmountValid());
    }

    @Test
    public void testIsNegativeNumberMinAmountValidate(){
        towarEntity.setMinAmount(-1);
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertFalse(iTowarValidateState.isMinAmountValid());
    }

    @Test
    public void testZeroNumberMinAmountValidate(){
        towarEntity.setMinAmount(0);
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertTrue(iTowarValidateState.isMinAmountValid());
    }

    @Test
    public void testMaxAmountSmallerThanMinAmountValidate(){
        towarEntity.setMinAmount(10);
        towarEntity.setMaxAmount(9);
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertFalse(iTowarValidateState.isMaxAmountValid());
    }


    @Test
    public void testMaxAmountEqualMinAmountValidate(){
        towarEntity.setMinAmount(10);
        towarEntity.setMaxAmount(10);
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertTrue(iTowarValidateState.isMaxAmountValid());
    }

    @Test
    public void testMaxAmountBiggerThanMinAmountValidate(){
        towarEntity.setMinAmount(10);
        towarEntity.setMaxAmount(11);
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertTrue(iTowarValidateState.isMaxAmountValid());
    }

    @Test
    public void testPassValidate(){
        towarEntity = new TowarEntity();
        towarEntity.setName("Beton Valon");
        towarEntity.setCode("CADCCCC123");
        towarEntity.setMinAmount(10);
        towarEntity.setMaxAmount(11);
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertTrue(iTowarValidateState.passValidate());
    }

    @Test
    public void testNotPassValidate(){
        towarEntity = new TowarEntity();
        towarEntity.setName("Beton Valon");
        towarEntity.setCode("CADCCCC12");
        towarEntity.setMinAmount(10);
        towarEntity.setMaxAmount(11);
        iTowarValidateState = towarValidatorService.validateTowar(towarEntity);
        assertFalse(iTowarValidateState.passValidate());
    }


}