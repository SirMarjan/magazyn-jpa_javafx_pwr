package logicLayer.logikaPrzegladaniaKatalogu;

import jxl.Sheet;
import jxl.Workbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import presentationLayer.przegladanieKatalogu.model.ITowarDetailsDTO;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


public class ExportToExcelServiceTest {
    private ExportToExcelService exportToExcelService;
    private List<ITowarDetailsDTO> towarDetailsDTOList;
    private File file;



    @Before
    public void initialize(){
        file = new File("test.xml");
        exportToExcelService = new ExportToExcelService();
        towarDetailsDTOList = null;
    }

    @After
    public void clean(){
        file.delete();
        file=null;

    }

    @Test
    public void testCorrectExportToExcelTest(){
        towarDetailsDTOList =
                Arrays.asList(
                        new TowarDetailsDTO(1,"Nazwa","CCCCCCCCCC",21,24,100,80,20,"litr","polskie, beton"),
                        new TowarDetailsDTO(2,"Nazwa2","CCCCCCCCC2",31,14,120,90,30,"tona","beton"));
        exportToExcelService.exportToExcel(towarDetailsDTOList,file);

        try (InputStream inputStream = new FileInputStream(file)) {
            Workbook workbook = Workbook.getWorkbook(inputStream);
            Sheet sheet = workbook.getSheet(0);
            assertEquals(sheet.getCell(0,1).getContents(),towarDetailsDTOList.get(0).getCode());
            assertEquals(sheet.getCell(1,1).getContents(),towarDetailsDTOList.get(0).getName());
            assertEquals(sheet.getCell(2,1).getContents(),towarDetailsDTOList.get(0).getMeasureString());
            assertEquals(Integer.parseInt(sheet.getCell(3,1).getContents()),towarDetailsDTOList.get(0).getMaxAmount());
            assertEquals(Integer.parseInt(sheet.getCell(4,1).getContents()),towarDetailsDTOList.get(0).getMinAmount());
            assertEquals(Integer.parseInt(sheet.getCell(5,1).getContents()),towarDetailsDTOList.get(0).getAvailableGoods());
            assertEquals(Integer.parseInt(sheet.getCell(6,1).getContents()),towarDetailsDTOList.get(0).getDepositedGoods());
            assertEquals(Integer.parseInt(sheet.getCell(7,1).getContents()),towarDetailsDTOList.get(0).getStoredGoods());
            assertEquals(sheet.getCell(8,1).getContents(),towarDetailsDTOList.get(0).getCategoriesString());

            assertEquals(sheet.getCell(0,2).getContents(),towarDetailsDTOList.get(1).getCode());
            assertEquals(sheet.getCell(1,2).getContents(),towarDetailsDTOList.get(1).getName());
            assertEquals(sheet.getCell(2,2).getContents(),towarDetailsDTOList.get(1).getMeasureString());
            assertEquals(Integer.parseInt(sheet.getCell(3,2).getContents()),towarDetailsDTOList.get(1).getMaxAmount());
            assertEquals(Integer.parseInt(sheet.getCell(4,2).getContents()),towarDetailsDTOList.get(1).getMinAmount());
            assertEquals(Integer.parseInt(sheet.getCell(5,2).getContents()),towarDetailsDTOList.get(1).getAvailableGoods());
            assertEquals(Integer.parseInt(sheet.getCell(6,2).getContents()),towarDetailsDTOList.get(1).getDepositedGoods());
            assertEquals(Integer.parseInt(sheet.getCell(7,2).getContents()),towarDetailsDTOList.get(1).getStoredGoods());
            assertEquals(sheet.getCell(8,2).getContents(),towarDetailsDTOList.get(1).getCategoriesString());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}